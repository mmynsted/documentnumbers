//
//  main.c
//  documentNumbers
//
//  Created by Mark Mynsted on 8/12/16.
//  Copyright © 2016 Mark Mynsted. All rights reserved.
//

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

void writeBin(uint32_t);
void writeX(uint32_t);
void writeBoth(uint32_t);

int main(int argc, char **argv)
{
    if (argc > 1) {
       for (int i = 1; i < argc; ++i)
       {
           long n = strtol(*(argv +i), (char **) NULL, 16);
           writeBoth((uint32_t) n);
       }
    }
    else {
        fprintf(stderr, "At least one hexadecimal argument is required.\n");
    }
    return 0;
}

void writeBoth(uint32_t n) {
    writeX(n);
    writeBin(n);
}

void writeX(uint32_t n) {
    printf("0x%08X ", n);
}

void writeBin(uint32_t n) {
    uint32_t c, k;
    c = 31;
    do
    {
        k = n >> c;
        if (k & 1)
            putchar('*');
        else
            putchar('.');
        if (c % 8 == 0) putchar(' ');
    } while (c-- !=0);
    putchar('\n');
}
